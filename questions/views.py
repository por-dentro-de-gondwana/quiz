# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.db.models import Count
from django.db import IntegrityError

from .forms import SignUpForm
from .models import Question, Score, Ranking, Player, Contest
from django.contrib.auth.models import User


# Main Page call
def index(request):
    result = Question.objects.all
    context = {'result': result}

    return render(request, 'questions/index.html', context)

# Accounts call (obsolete)
def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('index')
    else:
        form = SignUpForm()

    return render(request, 'registration/signup.html', {'form': form})

#Create a new player
def createPlayer(request):
    equipe = request.GET.get('equipe', '')
    if equipe:
        context = {
            'equipe': equipe
        }

    else:
        context = {}

    if request.method == 'POST':
        
        try:
            playerOneName = request.POST.get('txtPlayerOneName',None)
            playerTwoName = request.POST.get('txtPlayerTwoName',None)
            
            if not playerOneName:
                context.setdefault('erroNomeVazio','Nome de Jogador 1 não pode estar vazio.')
                output = render(request, 'registration/createplayer.html', context)
                return output 

            playersNames = [playerOneName]
            if playerTwoName:
                playersNames.append(playerTwoName)
            else:
                playerTwoName = None
            
            for playerName in playersNames:                     
                speakEnglish = request.POST.get('speakEnglish',None) in ['true', '1']

                player = Player(
                    name = playerName,
                    speakEnglish = speakEnglish
                )
                player.save()

            playerTwo = None
            if playerTwoName: 
                playerTwo = Player.objects.get(name = playerTwoName)

            contest = Contest(
                playerOne = Player.objects.get(name = playerOneName), 
                playerTwo = playerTwo  
            )
            contest.save()
            output =  redirect('/questions/?fase=1')
        
        except IntegrityError as error:
            context.setdefault('erroNomeCadastrado','Nome de Jogador existente.')
            output = render(request, 'registration/createplayer.html', context)
        
        
    else:
        output = render(request, 'registration/createplayer.html', context)
    
    return output 

# Congrats the players and show their Score
def thankyou(request):

    contest = Contest.objects.latest('id')
    try:
        if not contest.playerTwo:
            results = [Ranking.objects.get(player = contest.playerOne)]
        else:
            results = [Ranking.objects.get(player = contest.playerOne),Ranking.objects.get(player = contest.playerTwo)]
        
    except Ranking.DoesNotExist:
        results = None

    context = {'results': results}
    return render(request, 'questions/thankyou.html', context)

# Ranking with all players
def ranking(request):
    result = Ranking.objects.all().order_by('points').reverse()[:10]
    context = {'result': result}

    return render(request, 'questions/ranking.html', context)

# Game call
def questions(request):
    fase = request.GET.get('fase', '')
    if fase == "":
        return redirect('/questions/?fase=1')
    
    #TODO:Editar como é contruído o objeto player 
    contest = Contest.objects.latest('id')

    #
    result = Question.objects.filter(level = fase).order_by('order')
    context = {
        'result': result,
        'playerOne': contest.playerOne,
        'playerTwo': contest.playerTwo
    }
    output = render(request, 'questions/questions.html', context)

    if request.method == 'POST':

        player_name = request.POST.get('playerName',None)    
        question = request.POST.get('question',None)
        answer = request.POST.get('answer',None)       
        isCorrect = request.POST.get('isCorrect',None) in ['true', '1']

        score = Score(
            player = Player.objects.get(name = player_name),
            question = Question.objects.get(id = question),                
            answer = answer,
            isCorrect = isCorrect
        )
        score.save()

        if isCorrect:
            try:
                ranking = Ranking.objects.get(player = Player.objects.get(name = player_name))
            except Ranking.DoesNotExist:
                ranking = None

            if ranking:
                ranking.points += 10

            else:
                ranking = Ranking(
                    player = Player.objects.get(name = player_name),
                    points = 10
                )
            
            ranking.save()

    return output