# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import Level
admin.site.register(Level)

from .models import Question
admin.site.register(Question)

from .models import Player
admin.site.register(Player)

from .models import Contest
admin.site.register(Contest)

from .models import Score
admin.site.register(Score)

from .models import Ranking
admin.site.register(Ranking)