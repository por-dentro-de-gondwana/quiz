# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Level(models.Model):
        order = models.IntegerField(null=True)
        name = models.CharField(max_length=30)
        description = models.CharField(max_length=100)
        name_ingles = models.CharField(max_length=30, null=True)
        description_ingles = models.CharField(max_length=100, null=True)

        def __str__(self):
                return "%s | %s" % (self.order,self.name)

        def __unicode__(self):  # __unicode__ on Python 2
                return "%s | %s" % (self.order,self.name)

class Question(models.Model):
        order = models.IntegerField(null=True)
        qtext = models.CharField(max_length=100)
        infotext = models.CharField(max_length=700, null=True)
        qtext_ingles = models.CharField(max_length=100, null=True)
        infotext_ingles = models.CharField(max_length=700, null=True)
        answer = models.BooleanField()
        level = models.ForeignKey(Level, null=True, on_delete=models.CASCADE)

        def __str__(self):
                return "%s | %s [%s]" % (self.level.order, self.qtext, self.answer)

        def __unicode__(self):  # __unicode__ on Python 2
                return "%s | %s [%s]" % (self.level.order, self.qtext, self.answer)

class Player(models.Model):
        name = models.CharField(max_length=30, unique=True)
        speakEnglish = models.NullBooleanField(null=True)
        createDate = models.DateTimeField(auto_now_add=True, null=True)

        def __str__(self):
                return "%s criado em %s, selecionou %s para jogar em inglês." % (
                        self.name
                        ,self.createDate.strftime("%d de %b. de %Y - %I:%M %p")
                        ,self.speakEnglish
                )

        def __unicode__(self):  # __unicode__ on Python 2
                return "%s criado em %s, selecionou %s para jogar em inglês." % (
                        self.name
                        ,self.createDate.strftime("%d de %b. de %Y - %I:%M %p")
                        ,self.speakEnglish
                )

class Contest(models.Model):
        playerOne = models.ForeignKey(Player, related_name="playerOne", null=True)
        playerTwo = models.ForeignKey(Player, related_name="playerTwo", null=True)
        createDate = models.DateTimeField(auto_now_add=True, null=True)

        def __str__(self):
                return "Disputa entre %s VS %s, criada em %s ." % (
                        self.playerOne
                        ,self.playerTwo
                        ,self.createDate.strftime("%d de %b. de %Y - %I:%M %p")
                )

        def __unicode__(self):  # __unicode__ on Python 2
                return "Disputa entre %s VS %s, criada em %s ." % (
                        self.playerOne
                        ,self.playerTwo
                        ,self.createDate.strftime("%d de %b. de %Y - %I:%M %p")
                )


class Score(models.Model):
        player = models.ForeignKey(Player, null=True)
        question = models.ForeignKey(Question, null=True)
        answer = models.BooleanField()
        isCorrect = models.NullBooleanField(null=True)
        createDate = models.DateTimeField(auto_now_add=True)

        def __str__(self):
                return "%s respondeu %s para a pergunta '%s' na data %s" % (
                        self.player.name
                        ,self.answer 
                        ,self.question.qtext
                        ,self.createDate.strftime("%d de %b. de %Y - %I:%M %p")
                )

        def __unicode__(self):  # __unicode__ on Python 2
                return "%s respondeu %s para a pergunta '%s' na data %s" % (
                        self.player.name
                        ,self.answer 
                        ,self.question.qtext
                        ,self.createDate.strftime("%d de %b. de %Y - %I:%M %p")
                )

class Ranking(models.Model):
        player = models.ForeignKey(Player, null=True)
        points = models.PositiveIntegerField()
        createDate = models.DateTimeField(auto_now_add=True)
        updateDate = models.DateTimeField(auto_now=True)

        def __str__(self):
                return "%s acumulou %s pontos até a data %s" % (
                        self.player.name
                        ,self.points
                        ,self.updateDate.strftime("%d de %b. de %Y - %I:%M %p")
                )

        def __unicode__(self):  # __unicode__ on Python 2
                return "%s acumulou %s pontos até a data %s" % (
                        self.player.name
                        ,self.points
                        ,self.updateDate.strftime("%d de %b. de %Y - %I:%M %p")
                )