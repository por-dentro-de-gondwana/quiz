# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-12-04 23:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0014_auto_20171204_2330'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='name',
            field=models.CharField(max_length=30),
        ),
    ]
