# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-27 02:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0006_question_order'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='infotext',
            field=models.CharField(max_length=500, null=True),
        ),
    ]
