# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-11-09 19:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0010_ranking'),
    ]

    operations = [
        migrations.AddField(
            model_name='level',
            name='ingles',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='question',
            name='ingles',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
    ]
