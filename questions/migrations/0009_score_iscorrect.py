# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-03 02:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0008_auto_20170727_0234'),
    ]

    operations = [
        migrations.AddField(
            model_name='score',
            name='isCorrect',
            field=models.NullBooleanField(),
        ),
    ]
