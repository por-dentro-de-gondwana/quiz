"""quiz URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from questions import views as question_views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    # Question Views
    url(r'^$', question_views.index, name='index'),
    url(r'^create-player/$', question_views.createPlayer, name='createPlayer'),
    url(r'^questions/$', question_views.questions, name='questions'),
    url(r'^ranking/$', question_views.ranking, name='ranking'),
    url(r'^thankyou/$', question_views.thankyou, name='thankyou'),

    # 3rd Party Views
    url(r'^oauth/', include('social_django.urls', namespace='social')),
]

urlpatterns += staticfiles_urlpatterns()