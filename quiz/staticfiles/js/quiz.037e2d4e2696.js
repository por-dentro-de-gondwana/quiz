$(function(){
    $('.jq-btn').click(function(){
        processaResposta();
    });
});


function processaResposta(){
    $('#box-pergunta').addClass('box-resposta');
    $('.jq-menu-opcoes').addClass('hide');
    $('.jq-label-resposta').removeClass('hide');
    $('.jq-explicacao-resposta').removeClass('hide');

    //MOCK PARA VARIAR CERTO E ERRADO
    var classes = ["verdadeiro", "falso"];
    var resposta = classes[~~(Math.random()*classes.length)];
    $('#contexto-resposta').addClass(resposta);
    $('.jq-resposta').text(resposta);

    $('#jq-btn-proxima').removeClass('visibility-hidden');

}

function setFase(fase,pergunta){
    var index = pergunta-1;
    $('.pager-fase li').removeClass('active');
    $('.pager-fase').find('li').eq(index).addClass('active');
    
    $('body').attr('data-fase', fase);
    $('body').attr('data-pergunta', pergunta);

    $('body').removeClass(function (index, className) {
        return (className.match (/(^|\s)fase-\S+/g) || []).join(' ');
    });
    $('body').addClass('fase-'+fase);
    
    $('#jq-numero-pergunta-value').html(pergunta);
    

    //$.animeScene();
}