$(function(){
    $('.jq-btn').click(function(){
        processaResposta();
    });
});


function processaResposta(){
    $('#box-pergunta').addClass('box-resposta');
    $('.jq-menu-opcoes').addClass('hide');
    $('.jq-label-resposta').removeClass('hide');
    $('.jq-explicacao-resposta').removeClass('hide');

    //MOCK PARA VARIAR CERTO E ERRADO
    var classes = ["verdadeiro", "falso"];
    var resposta = classes[~~(Math.random()*classes.length)];
    
    $('#contexto-resposta').addClass(resposta);
    $('.jq-resposta').text(resposta);

    $('#jq-btn-proxima').removeClass('visibility-hidden');
}

function setFase(fase,pergunta){
    var index = pergunta-1;
    $('.pager-fase li').removeClass('active');
    $('.pager-fase').find('li').eq(index).addClass('active');
    
    $('body').attr('data-fase', fase);
    $('body').attr('data-pergunta', pergunta);

    $('body').removeClass(function (index, className) {
        return (className.match (/(^|\s)fase-\S+/g) || []).join(' ');
    });
    $('body').addClass('fase-'+fase);
    
    $('.jq-box-grupo').addClass('hide');
    var grupoPergunta = $('body').find('.jq-box-grupo').eq(index);
    $(grupoPergunta).removeClass('hide');

    $('.jq-box-pergunta').addClass('hide');
    var boxPergunta = $(grupoPergunta).find('.jq-box-pergunta');

    $(boxPergunta).find('.jq-numero-pergunta-value').html(pergunta);
    $.anime($(boxPergunta));

    $('.jq-menu-opcoes-lista').addClass('hide');
    var boxRespostas = $(grupoPergunta).find('.jq-menu-opcoes-lista');
    
    $.anime($(boxRespostas));
}

function proximaFase (numFase,numPergunta){
    setFase(numFase,numPergunta+1);
}