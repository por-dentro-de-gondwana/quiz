
class Animation{    
    anime(objeto){
      var animeClass = $(objeto).data('animation-class');
      $(objeto).addClass(animeClass);
    }
    animeCena(){
        var _Animation = this;        
        $(".anime" ).each(function( index ) {
           _Animation.anime($(this));
        });
    }
}

$.extend({
    anime: function(objeto) {
        var animeClass = $(objeto).data('animation-class');
        $(objeto).addClass(animeClass);
        $(objeto).removeClass('hide');
    },
    animeScene: function() {
        //var _Animation = this;        
        $(".anime").each(function( index ) {
            $.anime($(this));
        });        
    }
});