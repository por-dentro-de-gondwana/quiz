$(function(){
    $('.jq-btn').click(function(){
        //processaResposta();
    });
});


function processaResposta(resposta,perguntaAtual){

    $('#box-pergunta-'+perguntaAtual).addClass('box-resposta');
    
    $('#form-resposta-'+perguntaAtual).find('.jq-menu-opcoes').addClass('hide');
    $('#contexto-resposta-'+perguntaAtual).find('.jq-label-resposta').removeClass('hide');
    $('#contexto-resposta-'+perguntaAtual).find('.jq-explicacao-resposta').removeClass('hide');
    
    if (resposta == "Acertou") {
        $("#contexto-resposta-"+perguntaAtual).addClass("verdadeiro btn-verdadeiro");
    }else{
        $("#contexto-resposta-"+perguntaAtual).addClass("falso btn-falso");
    }
    
    var speakEnglish = $("#playerSpeakesEnglish").val();
    if (speakEnglish == "True") {
        if (resposta == "Acertou") {
            $("#contexto-resposta-"+perguntaAtual).find('.jq-resposta').text("Correct");    
        }else{
            $("#contexto-resposta-"+perguntaAtual).find('.jq-resposta').text("Wrong");
        }
    }else{
        $("#contexto-resposta-"+perguntaAtual).find('.jq-resposta').text(resposta);
    }
    //$("#contexto-resposta-"+perguntaAtual).find('.jq-resposta').text(resposta);
    $("#contexto-resposta-"+perguntaAtual).removeClass('hide');

    $('#jq-btn-proxima').removeClass('visibility-hidden');
}

function setFase(fase,pergunta){

    var totalFases = 4;
    var totalPerguntas = $('.jq-box-grupo').length;

    if(pergunta > totalPerguntas){
        var proximaFase = parseInt(fase) + 1;
        window.location.replace("/questions/?fase="+proximaFase);
    }

    if(fase > totalFases){
        window.location.replace("/thankyou");
    }
    
    $('body').addClass('pergunta');
    $('#jq-btn-proxima').addClass('visibility-hidden');
    
    var index = pergunta-1;
    $('.pager-fase li').removeClass('active');
    $('.pager-fase').find('li').eq(index).addClass('active');
    
    $('body').attr('data-fase', fase);
    $('body').attr('data-pergunta', pergunta);

    $('body').removeClass(function (index, className) {
        return (className.match (/(^|\s)fase-\S+/g) || []).join(' ');
    });
    $('body').addClass('fase-'+fase);
    
    $('.jq-box-grupo').addClass('hide');
    var grupoPergunta = $('body').find('.jq-box-grupo').eq(index);
    $(grupoPergunta).removeClass('hide');

    $('.jq-box-pergunta').addClass('hide');
    var boxPergunta = $(grupoPergunta).find('.jq-box-pergunta');

    $(boxPergunta).find('.jq-numero-pergunta-value').html(pergunta);
    

    $('.jq-menu-opcoes-lista').addClass('hide');
    var boxRespostas = $(grupoPergunta).find('.jq-menu-opcoes-lista');

    $('.jq-contexto-resposta').addClass('hide');
    
    

    if($('body').find('.transition-page').length > 0){
        $('body').find('.transition-page').addClass('fase-'+fase);
        
        //remover página de transicao
        setTimeout(function(){
            $('body').find('.transition-page').remove();
            $.anime($(boxPergunta));
            $.anime($(boxRespostas));
        }, 3000);
    }else{
        $.anime($(boxPergunta));
        $.anime($(boxRespostas));
    }


}

function proximaPergunta (numFase,numPergunta){
    setFase(numFase,numPergunta+1);
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}